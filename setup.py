from setuptools import setup

setup(
    name='raspberry-project',
    version='v0.1.0',
    packages=['raspberryproject', 'raspberryproject.avr'],
    url='',
    license='',
    author='Bill Tsagkas',
    author_email='bilarion@gmail.com',
    description=''
)
