import spidev
import time
import logging


class AVRController:

    ACK = 0x64
    ADC_REQUEST = 0x65
    DEFAULT_BUS = 0
    DEFAULT_SPI_DEVICE = 0
    DEFAULT_SPEED_HZ = 250000

    def __init__(self):
        # Initialize logging module.
        logging.basicConfig(format='[%(levelname)s] %(module)s: %(message)s', level=logging.DEBUG)

        self._spi = spidev.SpiDev()

    def init_spi(self):
        self.init_spi(self.DEFAULT_BUS,
                      self.DEFAULT_SPI_DEVICE,
                      self.DEFAULT_SPEED_HZ)

    def init_spi(self, bus, device, max_speed_hz):
        self._spi.open(bus, device)
        self._spi.max_speed_hz = max_speed_hz

    def read_adc(self):
        time.sleep(0.1)
        adc_low = self._spi.xfer2([self.ACK])[0]
        time.sleep(0.1)
        adc_high = self._spi.xfer2([self.ACK])[0]

        result = (adc_high << 8) | adc_low
        return result
