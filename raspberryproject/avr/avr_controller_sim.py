import logging
import random


class AVRControllerSim:

    def __init__(self):
        # Initialize logging module.
        logging.basicConfig(format='[%(levelname)s] %(module)s: %(message)s', level=logging.DEBUG)

    def init_spi(self):
        logging.debug("Initializing spi")

    def read_adc(self):
        logging.debug("Reading simulated adc value")
        # Generate and return a random value between 0 and 1023.
        return random.randint(0, 1024)
