import sys
import logging
from PyQt5.QtWidgets import QApplication, QDesktopWidget, QWidget, QGridLayout, QLineEdit, QPushButton, QLabel, QVBoxLayout, QHBoxLayout


def center(window):
    # Centers this window in the screen.
    qt_rectangle = window.frameGeometry()
    center_point = QDesktopWidget().availableGeometry().center()
    qt_rectangle.moveCenter(center_point)
    window.move(qt_rectangle.topLeft())


class Main:
    def __init__(self, debug):
        self.debug_mode = debug

        self.avr_controller = AVRController()
        self.avr_controller.init_spi()

        self.textfield = QLineEdit()
        self.window = self.init_ui()
        self.window.show()

    def init_ui(self):
        title = "RaspberryPi Project"
        if self.debug_mode:
            title += " (Debug Mode)"
        # Create the window.
        window = QWidget()
        window.resize(320, 200)
        window.setWindowTitle(title)

        # Parent layout.
        vbox = QVBoxLayout()

        # Top GridLayout.
        grid = QGridLayout()
        grid.setSpacing(10)

        adc_label = QLabel('Analog Value:')
        self.textfield.setReadOnly(True)
        grid.addWidget(adc_label, 0, 0)
        grid.addWidget(self.textfield, 0, 1)

        button = QPushButton('Read Analog Value')
        button.resize(button.sizeHint())
        button.clicked.connect(self.on_click)

        hbox = QHBoxLayout()
        hbox.addStretch(1)
        hbox.addWidget(button)

        vbox.addLayout(grid)
        vbox.addStretch(1)
        vbox.addLayout(hbox)

        window.setLayout(vbox)
        center(window)
        return window

    def on_click(self):
        adc_value = self.avr_controller.read_adc()
        self.textfield.setText(str(adc_value))


if __name__ == '__main__':
    # Initialize logging module.
    logging.basicConfig(format='[%(levelname)s] %(module)s: %(message)s', level=logging.DEBUG)

    # Read program arguments and set to 'Debug mode' if needed.
    if len(sys.argv) > 1 and sys.argv[1] in ['--debug', '-d']:
        logging.info("Debug mode enabled")
        debug_mode = True
        # If we're in debug mode, import the avr simulator instead.
        from raspberryproject.avr.avr_controller_sim import AVRControllerSim as AVRController
    else:
        debug_mode = False
        from raspberryproject.avr.avr_controller import AVRController as AVRController

    app = QApplication(sys.argv)
    main = Main(debug_mode)
    app.exec()
